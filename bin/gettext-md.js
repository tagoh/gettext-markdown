#! /bin/env node

'use strict';

const gmd = require('../libs/gettext-markdown.js');
const util = require('util');
const path = require('path');
const fs = require('fs');
const parseArgs = require('minimist');
const difflet = require('difflet')({indent: 2, comment: true});
const _ = require('underscore');
const colors = require('colors');
const pkg = require('../package.json');
const Glob = require('glob').Glob;

let argv = parseArgs(process.argv.slice(2), {
  string: ['o', 'd'],
  boolean: ['debug', 'h', 'v', 'pot', 'md', 'help']
});

if (argv.h || argv.help || argv._.length == 0) {
  console.log(util.format("Usage: %s <options> <*.md>|<podirs>", path.basename(process.argv[1])));
  const OPTIONS = `Options:
-h or --help	Show this help.
--debug		Enable debug option.
-o <FILE>	Write the result into FILE.
		__lang__ meta string will be replaced with the language
		and __name__ with the original base name.
-v		Validate the functionality
--pot		Generate POT from markdown documents
--md		Generate markdown documents from PO (default)
`;
  console.log(OPTIONS);
  process.exit(1);
}

let podir = argv.d || './po';
let ofile;

if (argv.v) {
  for (let i in argv._) {
    process.stdout.write("Validating " + argv._[i] + ' ...');
    let pot = gmd.md2pot(argv._[i]);
    gmd.po2md(path.basename(argv._[i], '.md') + '.pot', pot, {srcdir: path.dirname(argv._[i]), markdown: {debug: argv.debug}})
      .then((r) => {
        let _path = r[0].fn;
        let lang = r[0].lang;
        let data = r[0].data;
        let potctxt = fs.readFileSync(argv._[i], 'utf-8');
        let pottokens = gmd.lexer(potctxt.toString());
        let potokens = gmd.lexer(data.toString());

        if (_.isEqual(potokens, pottokens)) {
          process.stdout.write(colors.green('PASSED') + '\n');
        } else {
          process.stdout.write(colors.red('FAILED') + '\n');
          process.stdout.write('Please report this with the following information to ' + pkg.bugs.url + '\n');
          console.log('\nOriginal document');
          console.log('=================');
          console.log(potctxt);
          console.log('\nParsed document');
          console.log('===============');
          console.log(data);
          console.log('\nComparison');
          console.log('==========');
          let o = difflet.compare(pottokens, potokens);
          console.log(o);
          console.log('\nOriginal lexer');
          console.log('==============');
          console.log(pottokens);
          console.log('\nGenerated lexer');
          console.log('===============');
          console.log(potokens);
        }
      });
  }
} else if (argv.pot) {
  for (let i in argv._) {
    process.stdout.write("Generating POT for " + argv._[i]);
    let pot = gmd.md2pot(argv._[i]);
    let bname = path.basename(argv._[i], '.md');
    let dir = path.dirname(argv._[i]);
    ofile = argv.o || '__name__.pot';
    ofile = ofile.replace('__name__', bname).replace(/\.__lang__/, '');
    fs.writeFile(ofile, pot, (err) => {
      if (err) {
        process.stdout.write(' ... ' + colors.red('FAILED') + '\n');
        console.error(err);
      } else {
        process.stdout.write(' => ' + ofile + '\n');
      }
    });
  }
} else {
  for (let i in argv._) {
    let g = new Glob(path.join(argv._[i], '*\.po'), {nodir: true})
        .on('match', (fn) => {
          let po;

          try {
            po = fs.readFileSync(fn, 'utf-8');
          } catch (e) {
            process.stdout.write(fn + ' => ');
            process.stdout.write(colors.red('READ FAILED') + "\n");
            throw e;
          }
          gmd.po2md(fn, po, {markdown: {debug: argv.debug}})
            .then((r) => {
              for (let i in r) {
                let _path = r[i].fn;
                let lang = r[i].lang;
                let data = r[i].data;
                let pofn = r[i].po;
                let out = (argv.o || _path + '.__lang__').replace('__name__', path.basename(_path, '.md')).replace('__lang__', lang);
                fs.writeFile(out, data, 'utf-8', (e) => {
                  if (e) {
                    process.stdout.write(pofn + ' => ');
                    process.stdout.write(colors.red('WRITE FAILED') + "\n");
                    console.error(e);
                  } else {
                    process.stdout.write(pofn + ' => ');
                    process.stdout.write(out + "\n");
                  }
                });
              }
            });
        })
        .on('error', (e) => {
          g.abort();
          console.error(e);
        });
  }
}
