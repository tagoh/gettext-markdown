# gettext-markdown [![NPM version](https://badge.fury.io/js/gettext-markdown.png)](http://badge.fury.io/js/gettext-markdown)

> A Gettext implementation in JavaScript for Markdown document

## Installation

```
#!shell

% npm install gettext-markdown
```

## Getting Started with CLI

### Generate POT from the markdown

```shell
$ gettext-md -o /path/to/__name__.pot --pot /path/to/markdown.md
```

`__name__` will be replaced with `markdown` for the above case.

### Generate the localized markdown from translations

```shell
$ gettext-md -o /path/to/markdown/__name__.md.__lang__ --md /path/to/po/dirs
```

`/path/to/po/dirs` contains the gettext PO format files.
`__lang__` will be replaced with the certain language name according to PO files.

### If you doubt, try

```shell
$ gettext-md -v /path/to/markdown.md
```

This mode will validates if this library is capable to process the markdown properly.
if it fails, please feel free to report it according to the instructions.

## Getting Started with the code

### How to generate POT

```js
const gmd = require('gettext-markdown');

let pot = gmd.md2pot('/path/to/markdown.md');
```

`md2pot` returns the POT file contents.

### How to generate the localized markdown

```js
const gmd = require('gettext-markdown');
const fs = require('fs');

let po = fs.readFileSync('/path/to/gettext.po', 'utf-8');
gmd.po2md('/path/to/gettext.po', po)
  .then((r) => {
    for (let i in r) {
      let filename = r[i].fn;
      let pofilename = r[i].po;
      let lang = r[i].lang;
      let data = r[i].data;
      // fn contains the original markdown file name
      // po contains the PO file name
      // lang contains the language name for this PO
      // data contains the localized markdown contents
      ...
    }
  })
  .catch((e) => console.error(e));
```

### How to validate the markdown

```js
const gmd = require('gettext-markdown');

gmd.validate('/path/to/markdown.md')
  .then((resolve) => {
    // always returns true if it works
  }, (reject) => {
    // throw reject
  });
```

## License
Copyright (c) 2016 Akira TAGOH
Licensed under the [MIT license](LICENSE-MIT).

***

Project created by [Akira TAGOH](https://bitbucket.org/tagoh).
